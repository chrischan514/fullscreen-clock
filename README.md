# Fullscreen Clock
## About  
This application is written in Portuguese and optimised for Portuguese.  
This application only support iPads.  
I've now only tested on the 4th Gen iPad.
The ipa provided is not installable.
## Requirements
iOS version 10.3 or later.
## How to install
Open xcode and deploy the application onto your iPad.
//
//  ViewController.swift
//  Fullscreen Clock
//
//  Created by Chris Chan on 12/04/2019.
//  Copyright © 2019 Chris Chan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var currentTimeLabel: UILabel? {
        didSet {
            currentTimeLabel?.font = UIFont.monospacedDigitSystemFont(ofSize: 288.0, weight: UIFont.Weight.medium) // as big as possible
        }
    }
    
    @IBOutlet weak var secondLabel: UILabel? {
        didSet {
            secondLabel?.font = UIFont.monospacedDigitSystemFont(ofSize: 144.0, weight: UIFont.Weight.medium) // as big as possible
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel!
    var timer = Timer()
    var timerDate = Timer()
    var timerSecond = Timer()

    let showDate = DateFormatter()
    let showSecond = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
        timerDate = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.showDateLabel) , userInfo: nil, repeats: true)
        timerSecond = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector:#selector(self.showSecondLabel) , userInfo: nil, repeats: true)
    }
    @objc func tick() {
        currentTimeLabel?.text = DateFormatter.localizedString(from: Date(),
                                                              dateStyle: .none,
                                                              timeStyle: .short) //Show time with hour and minute
    }
    
    @objc func showDateLabel() {
        showDate.locale = Locale(identifier: "pt_PT")
        showDate.dateFormat = "EEEE, MMM d"
        dateLabel!.text = showDate.string(from: Date())
    }
    
    @objc func showSecondLabel() {
        showSecond.locale = Locale(identifier: "pt_PT")
        showSecond.dateFormat = "ss"
        secondLabel!.text = showSecond.string(from: Date())
    }

}

